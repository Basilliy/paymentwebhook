<?php

namespace App\Services;

use App\Models\Payments\UserWallet;
use App\Models\User;

class PaymentService extends BaseService
{
    public function sortRequest($request): bool
    {
        $user_wallet = UserWallet::query()->where('wallet_address', $request->address)->first();

        if(is_null($user_wallet)){
            return false;
        }

        $user = User::find($user_wallet->user_id);

        $this->resendRequest($user->branch, $request, 'CoinPaymentService');

        return true;
    }
}
