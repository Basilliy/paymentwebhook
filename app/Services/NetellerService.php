<?php

namespace App\Services;

class NetellerService extends BaseService
{

    public function sortRequest($request)
    {
        $branch = str_replace('_', '',substr($request->merchantRefNum, 0, 5));
        $request->merge(['merchantRefNum' => substr($request->merchantRefNum, 0, 5)]);

        $this->resendRequest($branch, $request, 'NetellerService');
    }
}
