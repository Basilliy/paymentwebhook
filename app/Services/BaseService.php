<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

abstract class BaseService
{
    abstract public function sortRequest($request);

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function resendRequest(string $branch, $request, string $serviceName)
    {
        if (!is_null($branch)){

            switch ($branch){
                case 'prod':
                    $res = str_replace('-XXX', '', Config::get('redirect.url'));
                    break;
                case 'dev':
                case 'stage':
                case strpos($branch, 'CBET-') !== false:
                    $res = str_replace('XXX', strtolower($branch), Config::get('redirect.url'));
                    break;
                case is_numeric($branch):
                    $res = str_replace('XXX', 'cbet-'.$branch, Config::get('redirect.url'));
                    break;
                default:
                    $res = str_replace('XXX', 'dev', Config::get('redirect.url'));
            }

            $this->apiCall(str_replace('{serviceName}', $serviceName, $res), $request->all());

        }
    }

    protected function apiCall(string $url, array $params)
    {
        $this->client->request('POST', $url, [
            'headers'     => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' =>
                $params,
        ]);
    }
}
