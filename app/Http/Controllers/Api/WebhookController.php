<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\NetellerService;
use App\Services\PaymentService;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    protected PaymentService $PaymentService;

    protected NetellerService $NetellerService;

    public function __construct(PaymentService $paymentService, NetellerService $netellerService)
    {
        $this->PaymentService = $paymentService;
        $this->NetellerService = $netellerService;
    }

    public function requestSort(Request $request, $serviceName)
    {
        $this->{$serviceName}->sortRequest($request);
    }
}
